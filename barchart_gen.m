%% Barchart
thresholds = [0.1, 0.3, 0.5];
chips  = {'chip1','chip2','chip3'};
outliers = zeros(12,3);
outliers(2,1) = 1;
outliers([2,3,6],2) = 1;
outliers([2,3],3) = 1;

exp_results = [];
exp_label = {};
list = [{1}, {2}, {1:2}];

color = {'red', 'green', 'blue'};
for t = 1:3
    idx = 1;
    for ch = 1:length(chips)
        for c = 2:3
            chip = chips{ch};
            load(sprintf('%s.mat', chip));
            for n = 1:length(df)
                % only calculte boxplot is using non-outlier results
                if outliers(n,ch) ~= 1
                    get_pen_depth(df{n}, thresholds(t), 'intcp');
                    tmp = cell2mat(getPropArray(df{n},'PcntPenDepth'))*100;
                    exp_results(idx) = tmp(end,c);

                    chip_data = split(df{n}(1,1).Label,'_');
                    run_label = chip_data{2};
                    chip_data = split(run_label, ' ');
                    exp_label(idx) =join([chip_data{1}(1), chip_data{1}(2), color(c)]);
                    idx = idx +1;                    
                end
            end
        end
    end
u = unique(exp_label);
for i = 1:length(u)
   mu_all(i) = mean(exp_results((contains(exp_label,u(i)))));
   std_all(i) = std(exp_results((contains(exp_label,u(i)))));
   label(i) = u(i);
end

%get colours for bar charts
base = repmat([0 0 0], 2,1);
base(1,3) = 1;
base(2,2) = 1;

h = figure('units','normalized','outerposition',[0.5 0.5 0.6 0.6]);

hb = bar(mu_all);
hold on
errorbar(1:length(mu_all), mu_all,std_all, 'or', 'MarkerSize', 8, 'LineWidth', 1);
set(gca,'XTick', 1:8,'XTickLabel', label)
set(gcf,'color','w');    
hb.FaceColor = 'flat';
hb.CData(:,:) = repmat(base, length(mu_all)/2,1);

ax=gca;
ax.FontSize = 14;
xtickangle(45)
ylim([0 110]);
yticks([0 20 40 60 80 100]);
ytickformat('percentage');
ylabel('Mean Penetration Depth (%)');
title_str = sprintf('All data at threshold=%d%%', thresholds(t)*100);
title(title_str, 'FontSize', 24)
tightfig() 

%     saveas(h,sprintf('%s_boxplot_comp_%g_th_%g.png', color, i, t))
end

%%