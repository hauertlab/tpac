 %% Calculate change in penetration over time
chips  = {'chip1','chip2','chip3'};
c_opt = [2 3];

threshold = 0.1;
run_label = {};
exp_results = [];
idx = 4;

outliers = zeros(12,3);
outliers(2,1) = 1;
outliers([2,3,6],2) = 1;
outliers([2,3],3) = 1;

h = figure('units','normalized','outerposition',[0 0 1 1]);
for c = 2:3
    if c == 3
        color = 'blue';
        clims = [0, 100];
    elseif c==2
        color = 'green';
        clims = [0, 100];
        
    end
    for i = 0:3
            subplot(1,4,mod(idx,4) + 1)
            mod(idx,4)
            trp_data = [];
            for ch = 1:length(chips)
                chip = chips{ch};
                load(sprintf('%s.mat', chip));
                for n = 1:3
                    if outliers(3*i + n, ch) == 1
                        chip_data = split(df{1,3*i + n}(1,1).Label,'_');
                        disp('outlier');
                    else
                        get_pen_depth(df{1,3*i + n}, threshold, 'intcp');
                        tmp = cell2mat(getPropArray(df{1,3*i + n},'PcntPenDepth'));  
                        trp_data = [trp_data, 100*tmp(:,c)];
                        chip_data = split(df{1,3*i + n}(1,1).Label,'_');
                    end
                end
            end
            [~, sz] = size(trp_data);
            mu_trp = mean(trp_data');
            SEM = std(trp_data')/sqrt(sz);
            ts = tinv([0.025  0.975],sz -1);
            CI = mu_trp + ts'*SEM;
            y_plot=[CI(1,:), fliplr(CI(2,:))];
            x_axis = 0.5:0.5:8;
            x_plot =[x_axis, fliplr(x_axis)];

            title(sprintf('threshold  = %d%%', threshold*100));
            chip_data = split(df{1,i*n + n}(1,1).Label,'_');
            
            semilogy(x_axis, mu_trp', 'black', 'linewidth', 1, 'DisplayName', chip_data{2})  
            hold on
%             fill(x_plot, y_plot, 1,'facecolor', color, 'edgecolor', 'none', 'facealpha', 0.4,'DisplayName', 'CI');
            xlabel('Time (hrs)')
            ylabel('Penetration depth (%)')
            legend show
%             ylim(clims);
            idx = idx+1;
    end
end
tightfig();